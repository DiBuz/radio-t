# Radio
Example technical test radio app.
Application has REST api.

* GET http://localhost:3000/songs - get all songs
* GET http://localhost:3000/smart-playlist generate smart playlist.
  * You can use query params:
    * conditionType {enum} - count or duration  
    * condition {number} - condition value number of songs or duration playlist in ms
     
Or you can use simply client
 http://localhost:3000/simple-client

Author Dima Buzenkov.