module.exports = {
	'env': {
		'mocha': true,
		'commonjs': true,
		'es6': true,
		'node': true,
		'jquery': true
	},
	'extends': [
		'eslint:recommended',
	],
	'parserOptions': {
		'ecmaVersion': 2017,
		'ecmaFeatures': {
			'experimentalObjectRestSpread': true,
		},
		'sourceType': 'module'
	},
	'plugins': [],
	'rules': {
		'indent': [
			'error',
			'tab',
			{
				'SwitchCase': 1
			}
		],
		'linebreak-style': [
			'error',
			'unix'
		],
		'quotes': [
			'error',
			'single'
		],
		'semi': [
			'error',
			'never'
		],
		'object-curly-spacing': ['error', 'always'],
		'array-bracket-spacing': ['error', 'always'],
		'no-console': 0,
	},
	'globals': {
		'models': false,
	}
}
