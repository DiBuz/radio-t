/* eslint-disable no-constant-condition */
const get = require('lodash/get')
const json = require('../env/library.json')
const seedrandom = require('seedrandom')
const debug = require('debug')('library')

const TEST_ENV = process.env.NODE_ENV === 'test'
const DEFAULT_EXECUTION_TIME = 500
const MAX_DIFFERENCE_TIME = 60 * 1000 // 1 minute
const COLLATOR = new Intl.Collator('co', { sensitivity: 'base' })


class LibraryError extends Error {
	constructor(message) {
		super(message)
	}
}

/**
 * Take the first letter in lowercase.
 * @param {string} string - Input strings
 * @param {boolean} [reverse=false] - Direction of search. False: from right to left. True: from left to right
 * @returns {string}
 */
function getLetter(string, reverse = false) {
	const letters = string.toLowerCase().trim().split('')
	const getFn = reverse ? 'pop' : 'shift'

	let letter

	while ((letter = letters[ getFn ]()) !== undefined) {
		if (/[^\d\s"'()[\]{}.,:;!?@#$%&*=+-]/ui.test(letter)) {
			return letter
		}
	}

	return undefined
}

/**
 * Add a song to the index by a unique string. Consider that 's' equal 'ś' etc.
 * @param {Library_Song} song - Song itself.
 * @param {string} uniqueString - Unique string associated with the song.
 * @param {Library_Index} index - Destination index.
 */
function songAddToIndex(song, uniqueString, index) {
	const [ , songs ] = Object
		.entries(index)
		.find(([ key ]) => COLLATOR.compare(uniqueString, key) === 0) || []

	if (!songs) {
		index[ uniqueString ] = [ song ]
		return
	}

	return songs.push(song)
}

/**
 * Find all songs by unique key. Consider that 's' equal 'ś' etc.
 * @param {string} uniqueString - Unique string associated with the songs.
 * @param {Library_Index} index - Source index
 * @return {Library_Song[]} - If nothing finds, it'l return empty array
 */
function getSongsByUniqueString(uniqueString, index) {
	const [ , songs = [] ] = Object
		.entries(index)
		.find(([ key ]) => COLLATOR.compare(uniqueString, key) === 0) || []

	return songs
}

/**
 *
 * @param {object} json - Raw data
 * @returns {Library}
 */
function init(json) {
	const artists = get(json, 'Library.Artists', [])

	const library = {
		firstLettersIndex: {},
		songs: []
	}

	for (const { name: artist, songs } of artists) {

		for (const { id, name, duration } of songs) {

			const firstLetter = getLetter(name, false)
			const lastLetter = getLetter(name, true)

			if (!firstLetter || !lastLetter) {
				console.warn(`Incorrect song name: [${id}] ${artist} - ${name}. It's skipped`)
				continue
			}

			const song = { id, artist, name, duration, firstLetter, lastLetter }

			songAddToIndex(song, firstLetter, library.firstLettersIndex)

			library.songs.push(song)
		}
	}

	return library
}

/**
 * Deep copy an index
 * @param {Library_Index} srcIndex - Source index
 * @return {Library_Index}
 */
function cloneIndex(srcIndex) {
	return Object
		.entries(srcIndex)
		.reduce((map, [ key, songs ]) => ({ ...map, [ key ]: songs.map(s => s) }), {})
}


/**
 * Generate smart playlist
 * @param {Library} library
 * @param {object} options
 * @param {string|number} [options.seed] - Random seed
 * @param {number} [options.condition=1] - Condition
 * @param {string} [options.conditionType='count'] - Condition type. Count: generate by total count. Duration : generate by total duration
 * @return {{playlist:Library_Song[], success:boolean}}
 */
function generateSmartPlaylist(library, { seed = Date.now(), condition = 1, conditionType = 'count', maxExecutionTime = DEFAULT_EXECUTION_TIME }) {
	if (!library.songs.length) {
		throw new LibraryError('Library is empty')
	}

	const totalSongs = library.songs.length
	const totalDuration = library.songs.reduce((total, { duration }) => total + duration, 0)

	switch (conditionType) {
		case 'count' :
			if (condition < 1 || totalSongs < condition) {
				throw new LibraryError(`Condition is wrong. Condition must be between ${1} and ${totalSongs}`)
			}
			break
		case 'duration': {
			if (condition > totalDuration) {
				throw new LibraryError(`Condition is wrong. Condition must be between ${1} and ${totalDuration} ms`)
			}
			break
		}
		default:
			throw new Error('Condition type is wrong')
	}

	debug(`Make smart playlist has started. Total songs ${totalSongs}, duration ${totalDuration} ms. Condition ${condition} type ${conditionType}. Seed: ${seed}`)

	const random = seedrandom(seed)
	const randomBetween = (max, min = 0) => Math.floor(random() * (max - min + 1)) + min

	let wrongWays = 0
	let fuse = Date.now()
	let bestVariant = []

	const step = (firstLettersIndex, playlist = []) => {
		if (Date.now() - fuse > maxExecutionTime) {
			debug(`Execution time has exceeded ${maxExecutionTime} ms`)
			throw new LibraryError('Execution time has exceeded')
		}

		switch (conditionType) {
			case 'count' :

				if (playlist.length === condition) {
					return playlist
				}
				if (bestVariant.length < playlist.length) {
					// debug(`I found best variant ${playlist.length} songs in place ${bestVariant.length} songs`)
					bestVariant = playlist
				}

				break
			case 'duration' : {
				const duration = playlist.reduce((total, { duration }) => total + duration, 0)
				const bestDuration = bestVariant.reduce((total, { duration }) => total + duration, 0)

				if (condition - MAX_DIFFERENCE_TIME <= duration && duration <= condition) {
					return playlist
				} else if (bestDuration < duration && duration <= condition) {
					// debug(`I found best variant by duration ${duration} ms in place ${bestDuration} ms`)
					bestVariant = playlist
				} else if (duration > condition) {
					return null
				}
				break
			}
		}

		const { lastLetter } = playlist[ playlist.length - 1 ]

		const suggestSongs = getSongsByUniqueString(lastLetter, firstLettersIndex)
			.filter(song => !playlist.includes(song))
			.map(song => song)

		do {
			if (suggestSongs.length === 0) {
				wrongWays++
				return null
			}

			const nextSongIndex = randomBetween(suggestSongs.length - 1)
			const [ nextSong ] = suggestSongs.splice(nextSongIndex, 1)

			const variant = step(firstLettersIndex, [ ...playlist, nextSong ], bestVariant)

			if (variant) {
				return variant
			}
		} while (true)
	}

	let firstSongs = library.songs.map(song => song)
	let playlist

	do {
		if (firstSongs.length === 0) {
			break
		}

		const firstSongIndex = randomBetween(firstSongs.length - 1)
		const [ firstSong ] = firstSongs.splice(firstSongIndex, 1)

		debug(`Try make playlist from first song: ${firstSong.artist} - ${firstSong.name}`)
		try {
			playlist = step(library.firstLettersIndex, [ firstSong ])
		} catch (exception) {
			if (exception.message === 'Execution time has exceeded') {
				break
			} else {
				throw exception
			}
		}

		if (playlist) {
			break
		}
	} while (true)

	if (!playlist) {
		debug(`Finished. Don't found right way. Return best variant. ${wrongWays} wrong ways`)
		return { playlist: bestVariant, success: false }
	} else {
		debug(`Finished. ${wrongWays} wrong ways`)
	}

	return { playlist, success: true }
}

const library = TEST_ENV ? {} : init(json)

module.exports = {

	LibraryError,

	... (TEST_ENV ? {
		getLetter,
		songAddToIndex,
		getSongsByUniqueString,
		cloneIndex,
		init,
		generateSmartPlaylist
	} : null),

	/**
	 * Get all the tracks
	 * @return {Library_Song[]}
	 */
	getSongs() {
		return library.songs
	},

	getSmartPlaylist(condition, conditionType) {
		return generateSmartPlaylist(library, { condition, conditionType })
	}
}


/**
 * @typedef {Object} Library_Song
 * @property {string} id - Song ID.
 * @property {string} artist - Artist name.
 * @property {string} name - Song title.
 * @property {number} duration - Song durations in milliseconds.
 * @property {string} firstLetter - Song title.
 * @property {string} lastLetter - Song title.
 */

/**
 * @typedef {Object.<string, Library_Song[]>} Library_Index
 */

/**
 * @typedef {Object} Library
 * @property {Library_Index} firstLettersIndex - Index by first letters.
 * @property {Library_Song[]} songs - All songs
 */