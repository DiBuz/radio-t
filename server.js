const PORT = 3000

const express = require('express')
const morgan = require('morgan')

const api = require('./routes')
const simpleClient = require('./simple-client')
const { errorHandling } = require('./middlewares')

const app = express()

app.use(morgan('tiny'))

app.use('/simple-client/', simpleClient)

app.use('/', api)

app.use(errorHandling())

app.listen(PORT, () => console.log(
	`Example technical test radio app.
Application has REST api.
 • GET http://localhost:${PORT}/songs - get all songs
 • GET http://localhost:${PORT}/smart-playlist generate smart playlist.
   - You can use query params, 
     conditionType {enum} - count or duration  
     condition {number} - condition value number of songs or duration playlist in ms
Or you can use simply client
 http://localhost:${PORT}/simple-client

Author Dima Buzenkov.
`))