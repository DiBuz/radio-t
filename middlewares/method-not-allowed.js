module.exports = (
	({ message = 'Method Not Allowed' } = {}) => (res, req) => req.status(405).end(message)
)