module.exports = {
	errorHandling: require('./error-handling'),
	methodNotAllowed: require('./method-not-allowed')
}