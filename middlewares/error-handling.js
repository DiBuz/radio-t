/* eslint-disable no-unused-vars */
module.exports = (
	({ message = 'Internal error', code = 500 } = {}) => (error, res, req, next) => {
		console.error('Unexpected error: ', error)
		req.status(code).end(message)
	}
)