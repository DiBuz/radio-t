const { expect } = require('chai')

const library = require('../models/library')

describe('Library', function () {
	const jsonLibrary = {
		'Library': {
			'Artists': [
				{
					'songs': [ {
						'name': '"Some" track"',
						'id': '1',
						'duration': 3 * 60 * 1000
					}, {
						'name': 'Foo bar"',
						'id': '4',
						'duration': 3 * 60 * 1000
					} ],
					'name': 'Some Artist'
				}, {
					'songs': [
						{
							'name': '101 Kind best "songś"',
							'id': '2',
							'duration': 5 * 60 * 1000
						},
						{
							'name': '($ Šome) music',
							'id': '3',
							'duration': 2 * 60 * 1000
						}
					],
					'name': 'Other Artist'
				}
			]
		}
	}

	describe('tools', function () {

		it('get first and last letter', function () {
			const testString = '" 100 Šome " track.'

			const first = library.getLetter(testString, false)
			const last = library.getLetter(testString, true)
			const empty = library.getLetter(' "(100)" ')

			expect(first).to.equal('š').and.lengthOf(1)
			expect(last).to.equal('k').and.lengthOf(1)
			expect(empty).to.be.undefined
		})

		describe('index tools', function () {
			const someSong = { id: '1', artist: 'Some artist', name: 'Some song', duration: 1 }
			const otherSong = { id: '1', artist: 'Some artist', name: 'Šome song', duration: 1 }
			const index = {}

			library.songAddToIndex(someSong, 'a', index)

			library.songAddToIndex(someSong, 'b', index)

			library.songAddToIndex(someSong, 's', index)
			library.songAddToIndex(otherSong, 'š', index)

			it('song add to index', function () {
				expect(index).to.have.property('a')
					.that.an('array')
					.that.is.length(1)

				expect(index).to.have.property('b')
					.that.an('array')
					.that.is.length(1)

				expect(index).to.have.property('s')
					.that.an('array')
					.that.is.length(2)

				expect(index).to.have.nested.property('s[1].name')
					.that.to.equal('Šome song')
			})

			it('get songs from index', function () {
				const songs = library.getSongsByUniqueString('š', index)

				expect(songs).to.an('array')
					.that.to.have.members([ someSong, otherSong ])
					.that.is.length(2)
			})

			it('clone index', function () {
				const copiedIndex = library.cloneIndex(index)

				expect(copiedIndex).to.not.equal(index)

				Object.keys(copiedIndex).forEach(key => {
					const songsArray = copiedIndex[ key ]
					expect(songsArray).to.not.equal(index[ key ])
					expect(songsArray[ 0 ]).to.equal(index[ key ][ 0 ])
				})
			})
		})

	})

	describe('Initialization and content', function () {

		const { songs, firstLettersIndex } = library.init(jsonLibrary)

		it('Check songs', function () {
			expect(songs).to.be.an('array').length(4)

			songs.forEach(song => {
				expect(song).to.have.property('id').that.is.a('string')
				expect(song).to.have.property('artist').that.is.a('string')
				expect(song).to.have.property('name').that.is.a('string')
				expect(song).to.have.property('duration').that.is.a('number')
				expect(song).to.have.property('firstLetter').that.is.a('string')
				expect(song).to.have.property('lastLetter').that.is.a('string')
			})
		})

		it('Check first latter index', function () {

			expect(firstLettersIndex).to.have.all.keys('s', 'k', 'f')

			const indexed = Object
				.entries(firstLettersIndex)
				.reduce((count, [ , songs ]) => count + songs.length, 0)

			expect(indexed).to.equal(songs.length, 'Not all songs hit the index')
		})

	})

	describe('Smart playlist', function () {
		const _library = library.init(jsonLibrary)

		it('By count', function () {

			expect(() => library.generateSmartPlaylist(_library, {
				condition: 5,
				conditionType: 'count'
			})).to.throw()

			const { playlist, success } = library.generateSmartPlaylist(_library, {
				seed: 1521461258911,
				condition: 3,
				conditionType: 'count'
			})

			expect(success).to.be.true
			expect(playlist)
				.to.an('array')
				.that.is.length(3)
		})

		it('By impossibly count', function () {

			const { playlist, success } = library.generateSmartPlaylist(_library, {
				seed: 1521461258911,
				condition: 4,
				conditionType: 'count'
			})

			expect(success).to.be.false

			expect(playlist)
				.to.an('array')
				.that.is.length(3)

			expect(playlist[ 0 ].lastLetter).to.equal(playlist[ 1 ].firstLetter)
		})

		it('By duration', function () {

			expect(() => library.generateSmartPlaylist(_library, {
				condition: 15 * 60 * 1000,
				conditionType: 'duration'
			})).to.throw()


			const { playlist, success } = library.generateSmartPlaylist(_library, {
				seed: 1521461258911,
				condition: 9 * 60 * 1000,
				conditionType: 'duration'
			}) || []

			expect(success).to.be.true

			expect(playlist)
				.to.an('array')
				.that.is.length(2)

			expect(playlist[ 0 ].lastLetter).to.equal(playlist[ 1 ].firstLetter)
		})
	})
})