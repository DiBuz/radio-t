const chai = require('chai')
const { expect } = chai
const chaiHttp = require('chai-http')
chai.use(chaiHttp)

const HOST = 'http://localhost:3000'

describe('Test REST API', function () {
	it('fails, as expected', function (done) {

		chai.request(HOST)
			.get('/smart-playlist')
			.query({ conditionType: 'count', condition: Number.MAX_SAFE_INTEGER })
			.end((err, res) => {
				expect(res).to.have.status(400)

				expect(res.body)
					.to.be.an('object')
					.that.to.have.property('error')
					.that.to.match(/Condition is wrong/)

				done()
			})
	})

	it('generate by count', function (done) {

		chai.request(HOST)
			.get('/smart-playlist')
			.query({ conditionType: 'count', condition: 10 })
			.end((err, res) => {
				expect(res).to.have.status(200)

				expect(res.body)
					.to.be.an('object')
					.to.have.property('playlist')
					.that.to.be.an('array').to.length(10)

				const { playlist } = res.body

				playlist.forEach(song => {
					expect(song).to.have.property('id').that.is.a('string')
					expect(song).to.have.property('artist').that.is.a('string')
					expect(song).to.have.property('name').that.is.a('string')
					expect(song).to.have.property('duration').that.is.a('number')
					expect(song).to.have.property('firstLetter').that.is.a('string')
					expect(song).to.have.property('lastLetter').that.is.a('string')
				})

				done()
			})
	})

	it('generate by duration', function (done) {
		// allowable range max - 1 min
		const minute = 60 * 1000
		const max = 10 * minute
		const min = max - (minute)

		chai.request(HOST)
			.get('/smart-playlist')
			.query({ conditionType: 'duration', condition: max })
			.end((err, res) => {
				expect(res).to.have.status(200)

				expect(res.body)
					.to.be.an('object')
					.to.have.property('playlist')
					.that.to.be.an('array')

				const { playlist } = res.body

				playlist.forEach(song => {
					expect(song).to.have.property('id').that.is.a('string')
					expect(song).to.have.property('artist').that.is.a('string')
					expect(song).to.have.property('name').that.is.a('string')
					expect(song).to.have.property('duration').that.is.a('number')
					expect(song).to.have.property('firstLetter').that.is.a('string')
					expect(song).to.have.property('lastLetter').that.is.a('string')
				})

				const duration = playlist.reduce((total, { duration }) => total + duration, 0)
				expect(duration)
					.to.be.least(min)
					.to.be.below(max)

				done()
			})
	})

})