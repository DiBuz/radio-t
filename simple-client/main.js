function durationToString(duration) {
	const SECOND = 1000
	const MINUTE = (60 * 1000)

	const min = Math.floor(duration / MINUTE)
	const sec = String(Math.floor((duration - (min * MINUTE)) / SECOND)).padStart(2, '0')

	return `${min}:${sec}`
}

function songToString({ artist, name, duration }) {
	return `• ${artist} - ${name} [${ durationToString(duration)}]`
}

$(() => {
	$('#generate_playlist')
		.on('submit', function (event) {
			event.preventDefault()
			const $form = $(this)
			const conditionType = $('select[name=condition_type]', $form).val()
			const condition = $('input[name=condition]', $form).val()

			$.getJSON({
				url: '/smart-playlist',
				data: { condition, conditionType },
			})
				.done((data) => {
					const { playlist } = data
					const duration = playlist.reduce((total, { duration }) => total + duration, 0)
					const count = playlist.length

					$('#info').html(`Count : ${count}, Duration: ${durationToString(duration)}  [${duration} ms]`)

					const list = playlist.map(song => `<li class="list-group-item">${songToString(song)}</li>`).join('')
					$('#playlist').html(list)
				})
				.fail(({ responseJSON }) => {
					$('#playlist').empty()
					if (responseJSON && responseJSON.error) {
						$('#info')
							.html(`<div class="alert alert-warning" role="alert">${responseJSON.error}</div>`)
					} else {
						$('#info')
							.html('<div class="alert alert-danger" role="alert">Unknown error!</div>')
					}
				})
		})
})