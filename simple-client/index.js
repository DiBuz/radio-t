const express = require('express')

const simpleClient = express.Router()

simpleClient.use(express.static(__dirname))

module.exports = simpleClient