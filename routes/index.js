const express = require('express')
const { methodNotAllowed } = require('../middlewares')

const { library } = require('../models')

const api = express.Router()

api.get('/smart-playlist', (req, res, next) => {
	const condition = Number(req.query.condition)
	const { conditionType = 'count' } = req.query

	try {
		const playlist = library.getSmartPlaylist(condition, conditionType)
		res.json(playlist)
	} catch (exception) {
		if (exception instanceof library.LibraryError) {
			res.status(400).json({ error: exception.message })
		} else {
			next(exception)
		}
	}

})

api.get('/songs', (req, res) => {
	res.json(library.getSongs())
})

api.use(methodNotAllowed())

module.exports = api